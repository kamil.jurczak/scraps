package pl.jurczak.kamil.app;

import lombok.SneakyThrows;
import pl.jurczak.kamil.dao.connection.AuthPropertiesResolver;

public class Main {

	@SneakyThrows
	public static void main(String[] args) {
		AuthPropertiesResolver.validateProperties();
		final var runner = new Runner();
		runner.start();
	}
}
