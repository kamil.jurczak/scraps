package pl.jurczak.kamil.app;

import pl.jurczak.kamil.app.command.factory.CommandFactory;
import pl.jurczak.kamil.app.util.PrinterProvider;
import pl.jurczak.kamil.app.util.ReaderProvider;

import static pl.jurczak.kamil.app.strings.StringConst.Message.ENTER_FILE_PATH;

public class Runner extends Thread {

	@Override
	public void run() {
		while (!Thread.currentThread().isInterrupted()) {
			final var writer = PrinterProvider.getWriter();
			writer.println(ENTER_FILE_PATH);
			final var reader = ReaderProvider.getReader();
			final var input = reader.readLine();
			final var command = CommandFactory.getCommandExecutor(input);
			command.execute();
		}
	}
}
