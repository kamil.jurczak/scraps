package pl.jurczak.kamil.app.command;

import pl.jurczak.kamil.app.util.ReaderProvider;
import pl.jurczak.kamil.app.util.ReaderWrapper;

public class ExitCommand extends Command {

	private final ReaderWrapper reader = ReaderProvider.getReader();

	@Override
	public void execute() {
		reader.close();
		Thread.currentThread().interrupt();
	}
}
