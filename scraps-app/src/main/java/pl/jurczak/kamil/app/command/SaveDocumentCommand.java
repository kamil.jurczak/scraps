package pl.jurczak.kamil.app.command;

import pl.jurczak.kamil.app.service.DocumentService;
import pl.jurczak.kamil.app.util.PrinterProvider;
import pl.jurczak.kamil.app.util.PrinterWrapper;
import pl.jurczak.kamil.app.util.ReaderProvider;
import pl.jurczak.kamil.app.util.ReaderWrapper;
import pl.jurczak.kamil.entity.Document;
import pl.jurczak.kamil.entity.Tag;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static pl.jurczak.kamil.app.strings.StringConst.Command.YES;
import static pl.jurczak.kamil.app.strings.StringConst.Message.*;
import static pl.jurczak.kamil.app.strings.StringConst.SpecialChars.COMMA;

public class SaveDocumentCommand extends Command {

	private final PrinterWrapper printer = PrinterProvider.getWriter();
	private final ReaderWrapper reader = ReaderProvider.getReader();
	private final File file;

	public SaveDocumentCommand(final File file) {
		this.file = file;
	}

	@Override
	public void execute() {
		if (file == null || !file.exists()) {
			printer.println(FILE_NOT_EXISTS);
			return;
		}
		final var document = new Document(file.getAbsolutePath());
		final var tags = tags();
		document.setTags(tags);
		saveDocument(document);
	}

	private List<Tag> tags() {
		printer.println(DO_YOU_WANT_ADD_TAGS);
		final var decision = reader.readLine();
		if (YES.equals(decision)) {
			printer.println(ENTER_TAGS_SEPARATE_BY_COMMA);
			final var input = reader.readLine();
			final var tags = input.split(COMMA);

			return Arrays.stream(tags)
					.map(String::trim)
					.map(Tag::new)
					.collect(Collectors.toList());
		}
		return Collections.emptyList();
	}

	private void saveDocument(final Document document) {
		final var success = DocumentService.save(document);
		if (!success) {
			printer.println(SOMETHING_WENT_WRONG_TRY_ONE_MORE_TIME);
		} else {
			printer.println(DOCUMENT_SAVED_SUCCESSFULLY);
		}
	}
}
