package pl.jurczak.kamil.app.command.factory;

import pl.jurczak.kamil.app.command.Command;
import pl.jurczak.kamil.app.command.ExitCommand;
import pl.jurczak.kamil.app.command.SaveDocumentCommand;

import java.io.File;

import static pl.jurczak.kamil.app.strings.StringConst.Command.QUIT;

public class CommandFactory {

	public static Command getCommandExecutor(String input) {
		return switch (input) {
			case QUIT -> new ExitCommand();
			default -> new SaveDocumentCommand(new File(input));
		};
	}
}
