package pl.jurczak.kamil.app.strings;

public class StringConst {

	private StringConst() {
	}

	public static final class Command {
		private Command() {
		}
		public static final String NO = "n";
		public static final String QUIT = ":q";
		public static final String YES = "y";
	}

	public static final class Message {
		private Message() {
		}
		public static final String DO_YOU_WANT_ADD_TAGS = "Do you want add tags? (y/n)";
		public static final String DOCUMENT_SAVED_SUCCESSFULLY = "Document saved successfully.";
		public static final String ENTER_FILE_PATH = "Enter file path:";
		public static final String ENTER_TAGS_SEPARATE_BY_COMMA = "Enter tags separate by comma:";
		public static final String FILE_NOT_EXISTS = "File not exists!";
		public static final String SOMETHING_WENT_WRONG_TRY_ONE_MORE_TIME = "Something went wrong. Please try one more time.";
	}

	public static final class SpecialChars {
		private SpecialChars() {
		}
		public static final String COLON = ":";
		public static final String COMMA = ",";
		public static final String DOT = ".";
		public static final String EMPTY = "";
		public static final String EXCLAMATION_MARK = "!";
		public static final String SPACE = " ";
		public static final String UNDERSCORE = "_";
	}
}
