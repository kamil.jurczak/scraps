package pl.jurczak.kamil.app.util;

import java.io.OutputStreamWriter;
import java.io.PrintWriter;

public class PrinterProvider {

	private PrinterProvider() {
	}

	public static PrinterWrapper getWriter() {
		final var printWriter = new PrintWriter(new OutputStreamWriter(System.out));
		return new PrinterWrapper(printWriter);
	}
}
