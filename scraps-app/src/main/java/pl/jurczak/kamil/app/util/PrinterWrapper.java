package pl.jurczak.kamil.app.util;

import lombok.AllArgsConstructor;

import java.io.PrintWriter;

@AllArgsConstructor
public class PrinterWrapper {

	private PrintWriter printWriter;

	public void println(String message) {
		printWriter.println(message);
		printWriter.flush();
	}
}
