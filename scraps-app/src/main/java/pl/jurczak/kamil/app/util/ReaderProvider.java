package pl.jurczak.kamil.app.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ReaderProvider {

	private ReaderProvider() {
	}

	public static ReaderWrapper getReader() {
		final var bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		return new ReaderWrapper(bufferedReader);
	}
}
