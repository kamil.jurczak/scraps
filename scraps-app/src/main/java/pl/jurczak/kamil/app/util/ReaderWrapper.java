package pl.jurczak.kamil.app.util;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;

import java.io.BufferedReader;

@AllArgsConstructor
public class ReaderWrapper {

	private BufferedReader bufferedReader;

	@SneakyThrows
	public String readLine() {
		return bufferedReader.readLine();
	}

	@SneakyThrows
	public void close() {
		bufferedReader.close();
	}
}
