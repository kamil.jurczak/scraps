package pl.jurczak.kamil.dao;

import org.hibernate.Transaction;
import pl.jurczak.kamil.dao.connection.HibernateUtil;
import pl.jurczak.kamil.entity.Document;

public class DocumentDao {

	private DocumentDao() {
	}

	public static boolean save(Document document) {
		var success = false;
		Transaction transaction = null;
		try (var session = HibernateUtil.getSessionFactory().openSession()) {
			transaction = session.beginTransaction();
			session.save(document);
			final var tags = document.getTags();
			tags.forEach(session::save);
			transaction.commit();
			success = true;
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
		return success;
	}
}
