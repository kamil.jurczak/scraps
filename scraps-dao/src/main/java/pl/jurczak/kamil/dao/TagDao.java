package pl.jurczak.kamil.dao;

import org.hibernate.Transaction;
import pl.jurczak.kamil.dao.connection.HibernateUtil;
import pl.jurczak.kamil.entity.Tag;

import java.util.List;

public class TagDao {

	private TagDao() {
	}

	public static boolean save(List<Tag> tags) {
		var success = false;
		Transaction transaction = null;
		try (var session = HibernateUtil.getSessionFactory().openSession()) {
			transaction = session.beginTransaction();
			tags.forEach(session::save);
			transaction.commit();
			success = true;
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
		}
		return success;
	}
}
