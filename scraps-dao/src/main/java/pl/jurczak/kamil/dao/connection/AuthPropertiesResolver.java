package pl.jurczak.kamil.dao.connection;

import lombok.SneakyThrows;
import pl.jurczak.kamil.dao.exception.UnresolvedDatabaseAuthPropertiesException;

import java.util.Properties;

public class AuthPropertiesResolver {

	private AuthPropertiesResolver() {
	}

	@SneakyThrows
	public static Properties getSuperUserProperties() {
		final var loader = Thread.currentThread().getContextClassLoader();
		final var stream = loader.getResourceAsStream("hibernateAuth.properties");
		if (stream == null) {
			throw new UnresolvedDatabaseAuthPropertiesException("File 'hibernateAuth.properties' not exists.");
		}
		final var properties = new Properties();
		properties.load(stream);
		return properties;
	}

	public static void validateProperties() throws UnresolvedDatabaseAuthPropertiesException {
		final var properties = getSuperUserProperties();
		final var user = properties.getProperty("db.user");
		final var password = properties.getProperty("db.password");
		if (user == null) {
			throw new UnresolvedDatabaseAuthPropertiesException("Cannot find 'db.user' property in 'hibernateAuth.properties'");
		} else if (password == null) {
			throw new UnresolvedDatabaseAuthPropertiesException("Cannot find 'db.password' property in 'hibernateAuth.properties");
		}
	}
}
