package pl.jurczak.kamil.dao.connection;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import pl.jurczak.kamil.entity.Document;
import pl.jurczak.kamil.entity.Tag;

import java.util.Properties;

import static org.hibernate.cfg.Environment.*;

public class HibernateUtil {

	private static SessionFactory sessionFactory;

	private HibernateUtil() {
	}

	public static SessionFactory getSessionFactory() {
		if (sessionFactory == null) {
			try {
				final var configuration = new Configuration();
				final var settings = new Properties();
				settings.put(DRIVER, "org.postgresql.Driver");
				settings.put(URL, "jdbc:postgresql://localhost:5432/scraps?useSSL=false");

				final var authProperties = AuthPropertiesResolver.getSuperUserProperties();
				settings.put(USER, authProperties.getProperty("db.user"));
				settings.put(PASS, authProperties.getProperty("db.password"));

				settings.put(DIALECT, "org.hibernate.dialect.PostgreSQL95Dialect");
				settings.put(SHOW_SQL, "true");
				settings.put(CURRENT_SESSION_CONTEXT_CLASS, "thread");
				settings.put(HBM2DDL_AUTO, "create-drop");
				configuration.setProperties(settings);
				configuration.addAnnotatedClass(Document.class);
				configuration.addAnnotatedClass(Tag.class);
				final var serviceRegistry = new StandardServiceRegistryBuilder()
						.applySettings(configuration.getProperties()).build();
				sessionFactory = configuration.buildSessionFactory(serviceRegistry);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return sessionFactory;
	}
}
