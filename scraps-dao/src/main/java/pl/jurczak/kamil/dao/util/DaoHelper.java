package pl.jurczak.kamil.dao.util;

import org.hibernate.Transaction;
import pl.jurczak.kamil.dao.connection.HibernateUtil;

public class DaoHelper {

	private DaoHelper() {
	}

	public static boolean saveRecord(Object entity) {
		var success = false;
		Transaction transaction = null;
		try (var session = HibernateUtil.getSessionFactory().openSession()) {
			transaction = session.beginTransaction();
			session.save(entity);
			transaction.commit();
			success = true;
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
		return success;
	}
}
