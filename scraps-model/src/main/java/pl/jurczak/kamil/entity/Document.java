package pl.jurczak.kamil.entity;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table
@Getter
public class Document {

	public Document(@NonNull final String path) {
		this.path = path;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NonNull
	private String path;

	@OneToMany
	@JoinColumn(name = "document_id")
	@Setter
	private List<Tag> tags;

	@Column(name = "create_on_utc")
	@CreationTimestamp
	private Timestamp createdOnUtc;

	@Column(name = "updated_on_utc")
	@UpdateTimestamp
	private Timestamp updatedOnUtc;
}
