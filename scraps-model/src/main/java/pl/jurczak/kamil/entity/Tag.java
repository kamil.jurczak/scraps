package pl.jurczak.kamil.entity;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table
public class Tag {

	public Tag(@NonNull final String name) {
		this.name = name;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NonNull
	private String name;

	@Column(name = "create_on_utc")
	@CreationTimestamp
	private Timestamp createdOnUtc;

	@Column(name = "updated_on_utc")
	@UpdateTimestamp
	private Timestamp updatedOnUtc;
}
