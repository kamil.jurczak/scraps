package pl.jurczak.kamil.app.service;

import pl.jurczak.kamil.dao.DocumentDao;
import pl.jurczak.kamil.entity.Document;

public class DocumentService {

	private DocumentService() {
	}

	public static boolean save(Document document) {
		return DocumentDao.save(document);
	}
}
