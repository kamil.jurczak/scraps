package pl.jurczak.kamil.app.service;

import pl.jurczak.kamil.dao.TagDao;
import pl.jurczak.kamil.entity.Tag;

import java.util.List;

public class TagService {

	private TagService() {
	}

	public static boolean save(List<Tag> tags) {
		return TagDao.save(tags);
	}
}
